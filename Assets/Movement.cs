﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
[RequireComponent(typeof(CharacterController))]
public class Movement : MonoBehaviour
{
    public float health = 100.0f;
    public float speed = 7.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float autorun = 0;
    public float normalVolume = 20.0f;
    public float highVolume = 100.0f;
    public float lowVolume = 10.0f;
    public float currentVolume;
    private Vector3 moveDirection = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        currentVolume = normalVolume;
    }

    // Update is called once per frame
    void Update()
    {

        CharacterController cc = GetComponent<CharacterController>();
        //Is Grounded
        if (cc.isGrounded)
        {
            moveDirection = new Vector3(0, 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            //Jump
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        moveDirection.y -= gravity * Time.deltaTime;
        cc.Move(moveDirection * Time.deltaTime);
        //Rotate Mouse
        transform.Rotate(0, Input.GetAxis("Mouse X"), 0);
        //Injured Walk
        if (health < 50)
        {
            normalVolume = 10;
            speed = 3;
        }
        else if (health > 50)
        {
            speed = 7;
        }
        //Run
        if (Input.GetKey(KeyCode.LeftShift))
        {
            currentVolume = highVolume;
            speed = 15;
            if (health < 50)
            {
                speed = 3;
            }
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            if (autorun == 0)
            {
                currentVolume = normalVolume;
                speed = 7;
            }
        }
        //Croach Speed/Slow Down
        if (Input.GetKey(KeyCode.LeftControl))
        {
            currentVolume = lowVolume;
            speed = 3;
        }

        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            if (autorun == 0)
            {
                currentVolume = normalVolume;
                speed = 7;
            }
        }
    }
}

