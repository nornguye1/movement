﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLock : MonoBehaviour
{
    public Transform player;

    public Vector3 offset;

    public float axisY;

    // Update is called once per frame
    void Update()
    {
        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * axisY, Vector3.up) * offset;
        //Updating on the position of the character and camera
        transform.position = player.position + offset;
        //Positioning to look at the player
        transform.LookAt(player.transform);
    }
}
