﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hearing : MonoBehaviour
{
    public Hearing heard;
    public float hearDistance = 10.0f;

    public float viewDistance = 10.0f;

    public float hearingScale = 10.0f;


    public float turnSpeed = 1;

    public float lookAroundTime = 5.0f;

    private Transform tf;

    public GameObject player;
    public enum States
    {
        Idle, LookAround, Chase
    }

    public States states;

    public float timeInCurrentState;

    private Transform playerTransform;

    private Transform aiTransform;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        heard = GetComponent<Hearing>();
    }

    // Update is called once per frame
    void Update()
    {
        timeInCurrentState += Time.deltaTime;
        switch (states)
        {
            case States.Idle:
                Idle();
                if (heard.CanHear(player.gameObject))
                {
                    states = States.LookAround;
                }

                break;
            case States.Chase:
                break;
            case States.LookAround:
                LookAround();
                if (timeInCurrentState > lookAroundTime)
                {
                    ChangeState(States.Idle);
                }
                break;
        }
    }

    void ChangeState(States newState)
    {
        states = newState;
        timeInCurrentState = 0;
    }
    void Idle()
    {

    }
    bool CanHear(GameObject target)
    {
        Movement movement = target.GetComponent<Movement>();
        if (movement == null)
        {
            return false;
        }

        // If they do, check the distance -- if it is <= (noise volume * hearingScale), then we can hear them!
        Transform targetTf = target.GetComponent<Transform>();
        if (Vector3.Distance(targetTf.position, tf.position) <= movement.currentVolume * hearingScale)
        {
            return true;
        }

        // Otherwise, we can't hear them
        return false;
    }

    public void LookAround()
    {
        Turn(true);
    }
    public void Turn(bool isTurnClockwise)
    {
        // Rotate based on turnSpeed and direction we are turning
        if (isTurnClockwise)
        {
            tf.Rotate(0, 0, turnSpeed * Time.deltaTime);
        }
        else
        {
            tf.Rotate(0, 0, -turnSpeed * Time.deltaTime);
        }
    }

}
